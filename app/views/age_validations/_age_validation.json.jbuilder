json.extract! age_validation, :id, :min, :max, :type, :created_at, :updated_at
json.url age_validation_url(age_validation, format: :json)
