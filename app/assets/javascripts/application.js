// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require jquery
//= require bootstrap-sprockets
//= require vendor/bootstrap-select.min
//= require vendor/jquery.slimscroll
//= require vendor/waves.min
//= require vendor/jquery.countTo
//= require vendor/raphael.min
//= require vendor/morris.min
//= require vendor/moment
//= require vendor/bootstrap-material-datetimepicker
// require vendor/Chart.bundle.min
// require vendor/flot-charts/jquery.flot
// require vendor/flot-charts/jquery.flot.resize
// require vendor/flot-charts/jquery.flot.pie
// require vendor/flot-charts/jquery.flot.categories
// require vendor/flot-charts/jquery.flot.time
//= require vendor/jquery.sparkline
//= require vendor/jquery.validate
//= require vendor/admin
//= require vendor/index
//= require vendor/demo
//= require vendor/waitMe.min
//= require vendor/jquery.fancybox.min
// require vendor/sweetalert.min
// require sweet-alert
// require sweet-alert-confirm
//= require tinymce.min
//= require vendor/dataTable/jquery.dataTables
//= require vendor/jquery.backstretch.min
//= require vendor/dataTable/dataTables.bootstrap.min
//= require vendor/dataTable/dataTables.buttons.min
//= require vendor/dataTable/buttons.flash.min
//= require vendor/dataTable/jszip.min
//= require vendor/dataTable/pdfmake.min
//= require vendor/dataTable/vfs_fonts
//= require vendor/dataTable/buttons.html5.min
//= require vendor/dataTable/buttons.print.min
//= require vendor/mindmup-editabletable
//= require vendor/previewForm

//= require custom

// require_tree .