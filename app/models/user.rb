# == Schema Information
#
# Table name: users
#
#  id                          :integer          not null, primary key
#  email                       :string           default(""), not null
#  encrypted_password          :string           default(""), not null
#  display_password            :string
#  reset_password_token        :string
#  reset_password_sent_at      :datetime
#  remember_created_at         :datetime
#  sign_in_count               :integer          default(0), not null
#  current_sign_in_at          :datetime
#  last_sign_in_at             :datetime
#  current_sign_in_ip          :inet
#  last_sign_in_ip             :inet
#  confirmation_token          :string
#  confirmed_at                :datetime
#  confirmation_sent_at        :datetime
#  unconfirmed_email           :string
#  country_live_id             :integer
#  name                        :string
#  country_id                  :integer
#  admin_id                    :integer
#  gender                      :integer
#  id_reg                      :string
#  passport                    :string
#  passport_expire             :date
#  dob                         :date
#  campus                      :string
#  phone                       :string
#  profession                  :string
#  win                         :boolean
#  lock                        :boolean          default(FALSE)
#  complete                    :boolean          default(FALSE)
#  note                        :text
#  skype_id                    :string
#  avatar_file_name            :string
#  avatar_content_type         :string
#  avatar_file_size            :integer
#  avatar_updated_at           :datetime
#  passport_image_file_name    :string
#  passport_image_content_type :string
#  passport_image_file_size    :integer
#  passport_image_updated_at   :datetime
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#

class User < ApplicationRecord
	include UploadValidations

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable

	attr_accessor :send_profile
	attr_accessor :sign_up
	attr_accessor :save_confirmation
	attr_accessor :kind_of_contest

	has_many :to_indonesias, dependent: :destroy
	has_many :bipa_courses, dependent: :destroy
	
	has_one :confirmation, dependent: :destroy
	has_one :score, dependent: :destroy

	accepts_nested_attributes_for :score

	belongs_to :admin, optional: true
	belongs_to :country_live, class_name: "Country", foreign_key:"country_live_id", optional: true
	belongs_to :country, class_name: "Country", foreign_key: "country_id", optional: true

	accepts_nested_attributes_for :to_indonesias, :bipa_courses, allow_destroy: true, reject_if: :all_blank
	
	enum gender: [ :lk, :pr]

	validates_presence_of :name, :country_id, :gender, :dob, :country_live_id,
												:campus, :phone, :profession, :avatar, if: :send_profile
	validates_presence_of :passport, :passport_expire, :passport_image, if: :save_confirmation
	
	validates_presence_of :dob, :name, :gender, if: :sign_up

	validates :passport, uniqueness: true, if: :save_confirmation
	validates :id_reg, uniqueness: true
	
	validate :valid_kind_contest, if: :sign_up
	# validate :valid_passport_expire, if: :save_confirmation
	 
	scope :completes, -> { where(complete: true) }
	
	# FOR FILTERS
	scope :search_complete, -> (complete) { where(complete: complete) }
	scope :search_year, -> (year) { where('extract(year  from users.created_at) = ?', year) }
	scope :search_kind, -> (kind) { joins(:score).where(scores:{kind: kind}) }
	scope :search_consulate, -> (consulate_id) { User.where(admin_id: consulate_id) }
	scope :search_win, -> (win) { where(win: win) }
	scope :search_country, -> (country_id) { User.where(country_id: country_id) }

	before_save :encrypt_password, if: :password
	after_create :build_additional
	
	# def valid_passport_expire
	# 	min = AgeValidation.find_by_kind(:passport).min
 #  	errors.add(:base, 'Tanggal kadaluarsa paspor harus minimal tanggal '+ min.strftime("%d/%m/%Y") ) if (min && passport_expire < min)
	# end

	def valid_kind_contest
  	errors.add(:base, 'Lomba harus diisi') if kind_of_contest.blank?
	end


	def count_to_indonesia
		if self.to_indonesias.present?
			day = []
			month = []

			self.to_indonesias.each do |dest|
				month.push(dest.long) if dest.unit.eql?("bulan")
				day.push(dest.long) if dest.unit.eql?("hari")
			end
			sum = day.inject(&:+) if day.present?
			month.delete_if{|d| d == nil || d == ""}
			sum = sum.to_i + month.inject(&:+)*30 if month.present?
			"(#{sum} Hari)"
		else
			""
		end
		
	end

  def encrypt_password
    crypt = ActiveSupport::MessageEncryptor.new(Rails.application.secrets.secret_key_base)
    encrypted_data = crypt.encrypt_and_sign(self.password)
    self.display_password = encrypted_data
  end
  
  def decrypt_password
    crypt = ActiveSupport::MessageEncryptor.new(Rails.application.secrets.secret_key_base)
    crypt.decrypt_and_verify(display_password) if display_password
  end

	def admin?
	  false
	end

	def registration_status
		schedule = Schedule.last
		now = DateTime.now
		if now >= (schedule.end_central_selection - 7.hour)
			if win == true
				"<span class='label bg-green'>Menang</span>".html_safe
			else
				"<span class='label bg-pink'>kalah</span>".html_safe
			end
		else
			if now <= (schedule.end_registration - 7.hour)
				"<span class='label bg-orange'>Masa Pendaftaran</span>".html_safe
			else
				"<span class='label bg-orange'>Masa Seleksi</span>".html_safe
			end
		end
		
	end

	def status_akun
		if confirmed? == true
			"<span class='label bg-green'>Aktif</span>".html_safe
		else
			"<span class='label bg-pink'>Belum Aktif</span>".html_safe
		end
	end

	def gender_print
		gender == "lk" ? "Laki-Laki" : "Perempuan"
	end
	
	def counting_age
	  now = Time.now
	  if self.dob.present?
	    dob = self.dob
	    now.year - dob.year - ((now.month > dob.month || (now.month == dob.month && now.day >= dob.day)) ? 0 : 1)
	  else
	    ""
	  end
	end

	def year
		created_at.year.to_s
	end

	def consulate
		admin
	end

	def self.by_current_year
    where('extract(year from created_at) = ?', DateTime.now.year)
	end

	protected

	def build_additional
		update_attributes(id_reg: "BIPA#{Time.now.strftime("%Y%m%d")}#{id.to_s.rjust(4, '0')}")
		score = self.build_score(kind: self.kind_of_contest)
		score.save(validate:false)
	end

end
