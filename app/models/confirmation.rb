# == Schema Information
#
# Table name: confirmations
#
#  id                          :integer          not null, primary key
#  flight_arrival_number       :string
#  flight_arrival_date         :date
#  flight_arrival_hours        :string
#  flight_return_number        :string
#  date_of_return_flight       :date
#  return_flight_hours         :string
#  hijab                       :boolean
#  dress_size                  :integer
#  script_file_name            :string
#  script_content_type         :string
#  script_file_size            :integer
#  script_updated_at           :datetime
#  arrival_ticket_file_name    :string
#  arrival_ticket_content_type :string
#  arrival_ticket_file_size    :integer
#  arrival_ticket_updated_at   :datetime
#  return_ticket_file_name     :string
#  return_ticket_content_type  :string
#  return_ticket_file_size     :integer
#  return_ticket_updated_at    :datetime
#  user_id                     :integer
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#  lock                        :boolean          default(FALSE)
#

class Confirmation < ApplicationRecord
  belongs_to :user
  enum dress_size: [ :xs, :s, :m, :l, :xl, :xxl, :xxxl, :xxxxl ]

  validates_presence_of :flight_arrival_number, :flight_arrival_date, :flight_arrival_hours, :flight_return_number,
												:date_of_return_flight, :return_flight_hours, :dress_size, :script_file_name, :script, :return_ticket, if: :lock
  validates :hijab, inclusion: { in: [ true, false ] }, if: :lock 

	has_attached_file :script 
	has_attached_file :arrival_ticket
	has_attached_file :return_ticket
	

  validates_attachment_content_type :script, content_type: ['image/jpeg', 'image/jpg', 'image/png', 'application/pdf'], message: 'Format Naskah salah', if: :script
  validates_attachment_content_type :arrival_ticket, content_type: ['image/jpeg', 'image/jpg', 'image/png', 'application/pdf'], message: 'Format tiket kedatangan salah', if: :arrival_ticket
  validates_attachment_content_type :return_ticket, content_type: ['image/jpeg', 'image/jpg', 'image/png', 'application/pdf'], message: 'Format tiket keberangkatan salah', if: :return_ticket

  validates_attachment :script, size: { in: 0..1.megabytes, message: 'Naskah harus kurang dari 1 megabytes' }, if: :script
  validates_attachment :arrival_ticket, size: { in: 0..1.megabytes, message: 'Tiket kedatangan harus kurang dari 1 megabytes' }, if: :arrival_ticket
  validates_attachment :return_ticket, size: { in: 0..1.megabytes, message: 'Tiket kepulangan harus kurang dari 1 megabytes' }, if: :return_ticket

  validates_attachment_file_name :script, matches: [/jpe?g\Z/, /png\Z/, /JP?G\Z/, /PNG\Z/, /PDF\Z/, /pdf\Z/  ], if: :script
  validates_attachment_file_name :arrival_ticket, matches: [/jpe?g\Z/, /png\Z/, /JP?G\Z/, /PNG\Z/, /PDF\Z/, /pdf\Z/  ], if: :arrival_ticket
  validates_attachment_file_name :return_ticket, matches: [/jpe?g\Z/, /png\Z/, /JP?G\Z/, /PNG\Z/, /PDF\Z/, /pdf\Z/  ], if: :return_ticket

  after_save :update_user_status

  def script_image?
    %w(image/jpeg image/jpg image/png).include?(script.content_type)
  end

  def script_pdf?
    %w(application/pdf).include?(script.content_type)
  end

  def arrival_ticket_image?
    %w(image/jpeg image/jpg image/png).include?(arrival_ticket.content_type)
  end

  def arrival_ticket_pdf?
    %w(application/pdf).include?(arrival_ticket.content_type)
  end

  def return_ticket_image?
    %w(image/jpeg image/jpg image/png).include?(return_ticket.content_type)
  end

  def return_ticket_pdf?
    %w(application/pdf).include?(return_ticket.content_type)
  end


	protected

	def update_user_status
		self.user.update_attributes(lock:true)
		# update_attributes(id_reg: "APLP-#{Time.now.strftime("%Y-%d-%m")}-#{id.to_s.rjust(4, '0')}")
	end
end
