# == Schema Information
#
# Table name: schedules
#
#  id                         :integer          not null, primary key
#  year                       :string
#  start_registration         :datetime
#  end_registration           :datetime
#  start_central_selection    :datetime
#  end_central_selection      :datetime
#  start_consulate_selection  :datetime
#  end_consulate_selection    :datetime
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  start_central_verification :datetime
#  end_central_verification   :datetime
#

class Schedule < ApplicationRecord
	validates_presence_of :year, :start_registration, :end_registration, :start_central_selection, :end_central_selection, :start_consulate_selection,
												:end_consulate_selection, :start_central_verification, :end_central_verification
	class << self
		def can_register?
			DateTime.now >= self.last.start_registration - 7.hour and Time.now <= self.last.end_registration - 7.hour
		end
		
		def can_sign_in?
			DateTime.now >= self.last.start_registration - 7.hour
		end

		def status
			schedule = Schedule.last
			now = DateTime.now
			if now >= (schedule.end_central_selection - 7.hour)
				"<h5 class='m-0'><b>Masa Pengumuman</b></h5>".html_safe
			else
				if now <= (schedule.end_registration - 7.hour)
					"<h5 class='m-0'><b>Masa Pendaftaran</b></h5>".html_safe
				else
					"<h5 class='m-0'><b>Masa Seleksi</b></h5>".html_safe
				end
			end
			
		end

	end

	# def start_registration
	# 	start_registration -7.hour
	# end

end
