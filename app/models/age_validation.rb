# == Schema Information
#
# Table name: age_validations
#
#  id         :integer          not null, primary key
#  min        :date
#  max        :date
#  kind       :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class AgeValidation < ApplicationRecord
	enum kind: [ :age, :passport ]

	def self.reset_default!
			AgeValidation.destroy_all
			AgeValidation.create(kind:0, min:"31 Jul 1988".to_date, max:"31 Jul 2000".to_date)
			AgeValidation.create(kind:1, max:"23 Oct 2018".to_date)
	end
end
