class Users::RegistrationsController < Devise::RegistrationsController
  include SimpleCaptcha::ControllerHelpers

  include Accessible
  before_action :authenticate_can_regis!

  skip_before_action :check_user, only: :destroy
  before_action :set_class

  # before_action :configure_sign_up_params, only: [:create]
  # before_action :configure_account_update_params, only: [:update]
  # GET /resource/sign_up
  def new
    super
  end

  # POST /resource
  def create
    if simple_captcha_valid?
      if validate_age
        begin
          super do
            resource.dob = localize_month(params["user"]["dob"]).to_date
            resource.save
          end
        rescue StandardError => error
          Rails.logger.info("=====LOGS:=======\n\n\n#{ error }\n\n\n==================")
          flash.now[:alert] = 'Maaf!, Masalah dengan email SMTP silakan coba lagi.'
          redirect_to new_user_registration_path
        end
      else
        flash.now[:alert] = 'Usia harus diantara 18 - 30 tahun per 31 Juli 2018'
        self.resource = resource_class.new sign_up_params
        respond_with_navigational(resource) { render :new }
      end
      
    else
      clean_up_passwords(resource)
      flash.now[:alert] = 'Recaptcha salah. Silhkan isi lagi kode.'
      self.resource = resource_class.new sign_up_params
      resource.validate # Look for any other validation errors besides Recaptcha
      respond_with_navigational(resource) { render :new }
    end

  end

  # GET /resource/edit
  # def edit
  #   super
  # end

  # PUT /resource
  # def update
  #   super
  # end

  # DELETE /resource
  # def destroy
  #   super
  # end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end
  def validate_age
      min =  AgeValidation.find_by_kind(:age).min #Date.new(1988, 7, 31)
      max =  AgeValidation.find_by_kind(:age).max #Date.new(2000, 7, 31)
      dob = localize_month(params["user"]["dob"]).to_date
      dob >= min && dob <= max
  end

  protected

  def set_class
    @body_class = "signup-page"
  end

  def authenticate_can_regis!
    unless Schedule.can_register?
      redirect_to root_url
      flash["alert"] = "Anda tidak berada pada masa seleksi"
    end
  end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_up_params
  #   devise_parameter_sanitizer.permit(:sign_up, keys: [:attribute])
  # end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_account_update_params
  #   devise_parameter_sanitizer.permit(:account_update, keys: [:attribute])
  # end

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   super(resource)
  # end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end
end
