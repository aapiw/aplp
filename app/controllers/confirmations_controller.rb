class ConfirmationsController < ApplicationController
  
  before_action :verify_user_winner
  # before_action :verify_winner

  before_action :set_var #, only: [:show, :edit, :update, :destroy]

  def index
  end

  def create
    if params["commit"] == "SIMPAN"
      @confirmation.lock = false
      @user.save_confirmation = false
    else
      @confirmation.lock = true
      @user.save_confirmation = true
    end

    confirmation_params_user = confirmation_params["user"]
    confirmation_params_user["passport_expire"] = localize_month(confirmation_params_user["passport_expire"]).to_date
    
    confirmation_params_edit = confirmation_params
    confirmation_params_edit.delete("user")
    confirmation_params_edit["flight_arrival_date"] = localize_month(confirmation_params_edit["flight_arrival_date"]).to_date
    confirmation_params_edit["date_of_return_flight"] = localize_month(confirmation_params_edit["date_of_return_flight"]).to_date

    @confirmation = Confirmation.new(confirmation_params_edit)
    if valid_passport_expire?
      if @user.update_attributes(confirmation_params_user)
        respond_to do |format|
          if @confirmation.save
            format.html { redirect_to present_confirmation_index_path }
            flash["notice"] = 'Konfirmasi berhasil diperbarui.'
          else
            format.html { redirect_to present_confirmation_index_path }
            flash["alert"] = @confirmation.errors.full_messages
          end
        end
        
        else
          respond_to do |format|
            format.html { redirect_to present_confirmation_index_path }
          end
          flash["alert"] = @user.errors.full_messages
        end
    else
      respond_to do |format|
        format.html { redirect_to present_confirmation_index_path }
      end
      flash["alert"] = "Tanggal kadaluarsa paspor harus minimal tanggal"+AgeValidation.find_by_kind(:passport).min.strftime("%d/%m/%Y")
    end
  end

  def update
    if params["commit"] == "SIMPAN"
      @confirmation.lock = false
      @user.save_confirmation = false
    else
      @confirmation.lock = true
      @user.save_confirmation = true
    end

    confirmation_params_user = confirmation_params["user"]
    confirmation_params_user["passport_expire"] = localize_month(confirmation_params_user["passport_expire"]).to_date
    
    confirmation_params_edit = confirmation_params
    confirmation_params_edit.delete("user")
    confirmation_params_edit["flight_arrival_date"] = localize_month(confirmation_params_edit["flight_arrival_date"]).to_date
    confirmation_params_edit["date_of_return_flight"] = localize_month(confirmation_params_edit["date_of_return_flight"]).to_date

    if valid_passport_expire?
      if @user.update_attributes(confirmation_params_user)
        respond_to do |format|
          if @confirmation.update(confirmation_params_edit)
            format.html { redirect_to present_confirmation_index_path }
            flash["notice"] = 'Konfirmasi berhasil diperbarui.'
          else
            format.html { redirect_to present_confirmation_index_path }
            flash["alert"] = @confirmation.errors.full_messages
          end
        end
      else
        respond_to do |format|
          format.html { redirect_to present_confirmation_index_path }
        end
        flash["alert"] = @user.errors.full_messages
      end
    else
      respond_to do |format|
        format.html { redirect_to present_confirmation_index_path }
      end
      flash["alert"] = "Tanggal kadaluarsa paspor harus minimal tanggal"+AgeValidation.find_by_kind(:passport).min.strftime("%d/%m/%Y")
    end

  end


  def valid_passport_expire?
    if params["commit"] != "SIMPAN"
      min = AgeValidation.find_by_kind(:passport).min
      min && localize_month(confirmation_params["user"]["passport_expire"]).to_date >= min
    else
      true
    end
  end

  private

    def verify_user_winner
      unless ( current_user && current_user.win && DateTime.now >= Schedule.last.end_central_selection - 7.hour )
        redirect_to root_url
        flash["alert"] = "Kamu tidak mempunyai Otoritas untuk mengakses halaman tersebut"
      end
    end

    def set_var

      @schedule = Schedule.last
      @user = current_user
      @confirmation =  @user.confirmation || Confirmation.new
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def confirmation_params
      params.require(:confirmation).permit(:flight_arrival_number, :lock, :flight_arrival_date, :flight_arrival_hours, :flight_return_number, :date_of_return_flight, :return_flight_hours, :hijab, :dress_size, :script, :arrival_ticket, :return_ticket, :user_id, 
        user: [:passport, :passport_image, :passport_expire, :save_confirmation] )
    end
end
