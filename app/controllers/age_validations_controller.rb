class AgeValidationsController < BaseController
  before_action :set_age_validation, only: [:show, :edit, :update, :destroy]

  # GET /age_validations
  # GET /age_validations.json
  def index
    @age_validation = AgeValidation.find_by_kind(:age)
    @age_validation.min = I18n.l(@age_validation.min, format: :long) if @age_validation.min
    @age_validation.max = I18n.l(@age_validation.max, format: :long) if @age_validation.max

    @passport_validation = AgeValidation.find_by_kind(:passport)
    @passport_validation.min = I18n.l(@passport_validation.min, format: :long) if @passport_validation.min
    @passport_validation.max = I18n.l(@passport_validation.max, format: :long) if @passport_validation.max

  end

  # GET /age_validations/1
  # GET /age_validations/1.json
  def show
  end

  # GET /age_validations/1/edit
  def edit
  end

  # POST /age_validations
  # POST /age_validations.json
  # def create
  #   @age_validation = AgeValidation.new(age_validation_params)

  #   respond_to do |format|
  #     if @age_validation.save
  #       format.html { redirect_to @age_validation, notice: 'Age validation was successfully created.' }
  #       format.json { render :show, status: :created, location: @age_validation }
  #     else
  #       format.html { render :new }
  #       format.json { render json: @age_validation.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # PATCH/PUT /age_validations/1
  # PATCH/PUT /age_validations/1.json
  def update
    age_validation_params_edit = age_validation_params
    # age_validation_params_edit["min"] = localize_month(age_validation_params_edit["min"]).to_date
    age_validation_params_edit["min"] = localize_month(age_validation_params_edit["min"]).to_date

    respond_to do |format|
      if @age_validation.update(age_validation_params_edit)
        format.html { redirect_to age_validations_path, notice: 'Validasi Berhasil di update.' }
      else
        format.html { render :index }
      end
    end
  end

  # DELETE /age_validations/1
  # DELETE /age_validations/1.json
  # def destroy
  #   @age_validation.destroy
  #   respond_to do |format|
  #     format.html { redirect_to age_validations_url, notice: 'Age validation was successfully destroyed.' }
  #     format.json { head :no_content }
  #   end
  # end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_age_validation
      @age_validation = AgeValidation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def age_validation_params
      params.require(:age_validation).permit(:min, :max, :kind)
    end
end
