module ApplicationHelper
	# def root_path
	# 	if user_signed_in?
	# 		users_dashboard_index_path
	# 	else
	# 		admins_dashboard_index_path
	# 	end
	# end
	
	def active_class(link_path, link_path2=nil)
		if link_path.present? and link_path2.present?
			if current_page?(link_path)
				"active"		
			elsif current_page?(link_path2)
				"active"
			end
		else
		 current_page?(link_path) ? "active" : "" 
		end
	end

	def show(exp=nil, true_text=nil, false_text=nil)
		# if text == "lock"
		# 	text = 
		# end
		klass = exp.class
		html = ""
		if klass == String
			if exp == "Belum Dikirim"
				html = "<span class='label bg-orange'>Belum Dikirim</span>"
			elsif exp == "Dikirim" 
				html = "<span class='label bg-green'>Dikirim</span>"
			elsif exp == "" 
				html = "<span class='label bg-orange'>Kosong</span>"
			else
				return exp.titlecase rescue nil
			end
		elsif klass == TrueClass
			html = "<span class='label bg-green'>#{true_text ? true_text : 'Lulus' }</span>"
		elsif klass == FalseClass
				html = "<span class='label bg-orange'>#{false_text ? false_text : 'Tidak Lulus' }</span>"
		elsif klass == NilClass
				html = "<span class='label bg-orange'>Kosong</span>"
			else
				return exp rescue nil
		end
		html.html_safe
	end

	def required field=nil
		"<span class='col-pink'> (harus diisi)</span>".html_safe if field.blank?
	end
	
	def get_bgs
		1.upto(15).to_a.shuffle.map{ |e| asset_path('apresiasi/bg-'+e.to_s+'.JPG') } 
	end

	def yes_no field
		if field
			"<span class='label bg-green'>Ya</span>".html_safe 
		else
			"<span class='label bg-orange'>Tidak</span>".html_safe 
		end
		
	end
	
	def min_seven date
		date - 7.hour
	end
end
