class CreateAgeValidations < ActiveRecord::Migration[5.1]
  def change
    create_table :age_validations do |t|
      t.date :min
      t.date :max
      t.integer :kind

      t.timestamps
    end
  end
end
