class ChangeStringToText < ActiveRecord::Migration[5.1]
  def change
  	change_column :landings, :title, :text
  end
end
