class AddStartEndVerificationToSchedule < ActiveRecord::Migration[5.1]
  def change
  	add_column :schedules, :start_central_verification, :datetime
  	add_column :schedules, :end_central_verification, :datetime
  end
end
