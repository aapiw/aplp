class AddLockToConfirmation < ActiveRecord::Migration[5.1]
  def change
  	add_column :confirmations, :lock, :boolean, default: false
  end
end
