class AddAttendtoCountryAndremoveMajorsFromUser < ActiveRecord::Migration[5.1]
  def change
  	add_column :countries, :attend_contest, :boolean, default: false
  	remove_column :users, :majors
  end
end
